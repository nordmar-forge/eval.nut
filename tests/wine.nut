/*
    There is example functions. You can execute them with using 'sveval FunctionName()'
    It can be useful for admins or debuging some staff
*/

toggleScriptEval(true)

if (SERVER_SIDE)
{
    local drinkCount = 0

    function GiveWine(amount = 1)
    {
        giveItem(0, Items.id("ITFO_WINE"), amount)
        Eval.Utils.LogMessage("I give wine!")
    }

    function DrinkWine()
    {
        useItem(0, Items.id("ITFO_WINE"))
        drinkCount++
        Eval.Utils.LogMessage("W pojontku")
    }
}

/*
USE:
    sveval GiveWine(3)
    sveval DrinkWine()
    sveval print("I drink... " + drinkCount + " times...")
    sveval setPlayerPosition(0, 500, 200, 4000)
*/