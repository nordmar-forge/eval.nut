# eval.nut
Evaluate/Run script from string content by console commands. It can be useful for admins or debuging some stuff.

# Installation

## First step
### Classic

gg wp ez :thumbsub:

### Git

Write `git clone https://gitlab.com/nordmar-forge/eval.nut.git --recursive` into yours terminal/cmd.

Module was require [BPackets](https://gitlab.com/bcore1/bpackets.git) scripts for network messages.

## Second step
Edit yours `config.xml` in server folder and add
```xml
<import src="eval.nut/meta.xml" />
```

By default module was disabled.
You should enable module by using `toggleScriptEval(true)` (or `Eval.Manager.ToggleScriptEval`) in both side scripts (server/client).

# API

**toggleScriptEval** `(toggle: boolean)` - Enable or disable eval commands feature

## Console commands API

`cleval [script]` - Run script from client side.

`sveval [script]` - Run script from server side.