/*
    Evaluate/Run script from string content by console commands

    Made in abyss by DocNight ()_()
*/



# Eval Namespace start

Eval <- {}

# Eval Namespace end



# Eval GlobalAPI start

// Constants
EVAL_CLIENT_CMD <- "cleval" // i dont know better idea for names (-_-)
EVAL_SERVER_CMD <- "sveval"

# Eval GlobalAPI end



# Eval Network start

class Eval.ScriptMessage extends BPacketMessage
{
    </ type = BPacketString />
    script = ""
}

# Eval Network end



# Eval Manager start

class Eval.Var
{
  Value = null

  constructor(value)
  {
    this.Value = value
  }
}

class Eval.Manager
{
    static Enabled = Eval.Var(false)

    static function ToggleScriptEval(toggle)
    {
        enabled = toggle
    }
}

toggleScriptEval <- @(toggle) Eval.Manager.ToggleScriptEval

# Eval Manager end



# Eval Utils start

class Eval.Utils
{
    static function LogMessage(msg)
    {
        local si = getstackinfos(1)
        print(si.src + ": " + msg)
    }

    static function Execute(source)
    {
        if (!Eval.Manager.Enabled)
            return

        local script = compilestring(source)
        script()
    }
}

local LogMessage = @(msg) Eval.Utils.LogMessage(msg)

# Eval Utils end



# Eval Main start

local function ClientMain()
{
    local function evalServer(params)
    {
        local netmsg = Eval.ScriptMessage(params)
        netmsg.serialize().send(RELIABLE)
    }

    addEventHandler("onInit", function ()
    {
        LogMessage("ClientMain was initialized!")
    })

    addEventHandler("onConsole", function (command, params)
    {
        if (!Eval.Manager.Enabled)
            return

        switch (command) {
            case EVAL_CLIENT_CMD:
                Eval.Utils.Execute(params)
                break;
            case EVAL_SERVER_CMD:
                evalServer(params)
                break;
            default:
                return
        }
        // Should be call for removing default error message
        eventValue(CONSOLE_COMMAND_FOUND)
    })
}

local function ServerMain()
{
    Eval.ScriptMessage.bind(function (pid, netmsg)
    {
        Eval.Utils.Execute(netmsg.script)
    })

    addEventHandler("onInit", function ()
    {
        LogMessage("ServerMain was initialized!")
    })
}

# Eval Main end



# Eval Startup start

if (CLIENT_SIDE)
{
    ClientMain()
}
else
{
    ServerMain()
}

# Eval Startup end



# Eval Log start
/* I dunno, maybe sometime i enable it...
addEventHandler("onInit", function ()
{
    print("\n# Eval commands was avaible! #\n")
})
*/
# Eval Log end